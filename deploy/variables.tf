variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "eduard.correa.avendano@segurosbolivar.com"
}

variable "db_username" {
  description = "Username for the RDS Postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR Image for API"
  default     = "238029452359.dkr.ecr.us-east-2.amazonaws.com/recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR Image for Proxy"
  default     = "238029452359.dkr.ecr.us-east-2.amazonaws.com/recipe-app-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}

variable "layer" {
  description = "Layer"
  default     = "ciencuadras"
}

variable "stack_id" {
  description = "Stack"
  default     = "dev"
}

variable "api_auth_scopes" {
  type = map
  default = {
    scope1 = {
      "scope_name" : "read"
      "scope_description" : "Read"
    },
    scope2 = {
      "scope_name" : "write"
      "scope_description" : "Write"
    }
  }
}
