resource "aws_cognito_user_pool" "pool" {
  name = "${var.layer}-${var.stack_id}-api-auth-pool"

  tags = local.common_tags
}

resource "aws_cognito_resource_server" "resource" {
  identifier = "${var.layer}-${var.stack_id}-api-server"
  name       = "${var.layer}-${var.stack_id}-api-server"

  dynamic "scope" {
    for_each = [for key, value in var.api_auth_scopes : {
      scope_name        = value.scope_name
      scope_description = value.scope_description
    }]

    content {
      scope_name        = scope.value.scope_name
      scope_description = scope.value.scope_description
    }
  }
  user_pool_id = aws_cognito_user_pool.pool.id
}

resource "aws_cognito_user_pool_client" "client" {
  name                                 = "${var.layer}-${var.stack_id}-api-auth-client"
  generate_secret                      = true
  refresh_token_validity               = 1
  allowed_oauth_flows_user_pool_client = true
  explicit_auth_flows                  = ["ALLOW_ADMIN_USER_PASSWORD_AUTH", "ALLOW_CUSTOM_AUTH", "ALLOW_USER_SRP_AUTH", "ALLOW_REFRESH_TOKEN_AUTH"]
  allowed_oauth_flows                  = ["client_credentials"]
  allowed_oauth_scopes                 = aws_cognito_resource_server.resource.scope_identifiers
  user_pool_id                         = aws_cognito_user_pool.pool.id
}

resource "aws_cognito_user_pool_domain" "domain" {
  domain       = "${var.layer}-${var.stack_id}-api-auth"
  user_pool_id = aws_cognito_user_pool.pool.id
}

